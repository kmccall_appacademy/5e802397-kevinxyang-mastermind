class Code

  PEGS = {
    "r" => :red,
    "y" => :yellow,
    "g" => :green,
    "b" => :blue,
    "o" => :orange,
    "p" => :purple,
  }

  attr_reader :pegs

  # initialize will accept an array of pegs
  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    rando_pegs = PEGS.keys.shuffle
    Code.new(rando_pegs[0..3])
  end

  # it will parse the array of pegs into an array
  def self.parse(string)
    return_pegs = []
    string.downcase.chars do |color|
      raise "Invalid color." unless PEGS.has_key?(color)
      return_pegs << color
    end

    Code.new(return_pegs)
  end

  # LEARN THE BELOW and WHAT THEY DO
  def [](i)
    pegs[i]
  end


  def exact_matches(other_code)
    perfect_matches = 0

    @pegs.each_index do |i|
      perfect_matches += 1 if @pegs[i] == other_code[i]
    end

    perfect_matches
  end

  def near_matches(other_code)
    close_matches = 0

    @pegs.each_index do |i|
      close_matches += 1 if @pegs[i] != other_code[i] && @pegs[i...pegs.length].include?(other_code[i])
    end

    close_matches
  end

  def ==(other_code)
    return false unless other_code.is_a?(Code)

    self.pegs == other_code.pegs
  end

end


class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  # you can set a variable equal to the return value of a method
  def play
    i = 0
    while i < 10
      user_guess = get_guess
      @secret_code.display_matches(user_guess)
      break if user_guess == @secret_code
    end
  end

  def get_guess
    puts "Put in the four colors you think it is: "
    # user_guess =
    Code.parse(gets.chomp)

    puts "Guess the code:"
  end

  def display_matches(user_guess)
    exact_matches = @secret_code.exact_matches(user_guess)
    near_matches = @secret_code.near_matches(user_guess)

    print "Your exact matches: #{exact_matches}"
    print "Your near matches: #{near_matches}"
  end

end
